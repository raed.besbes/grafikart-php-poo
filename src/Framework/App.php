<?php
namespace Framework;

use GuzzleHttp\Psr7\Response;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

class App
{
    public function run(ServerRequestInterface $request): ResponseInterface
    {
        //methode 1 sans Guzzl
        //$uri = $_SERVER['REQUEST_URI'];
        //Methode 1 sans Guzzl
        /*header('Location: '. substr($uri, 0, -1));
        header('HTTP/1.1 301 Moved Permanently');*/
        /* $response = new Response();
      $response->getBody()->write('Bonjour Takiacademy');
      return $response;*/
        //echo 'Bonjour Takiacademy';
        // exit();
        $uri = $request->getUri()->getPath();
        //$uri[-1] : dernier caractère
        if (!empty($uri) && $uri[-1] === "/") {
            return (new Response())
                ->withStatus(301)
                ->withHeader('Location', substr($uri, 0, -1));
            return $response;
        }
        if ($uri === '/blog') {
            //[] : c a d pas de header
            return new Response(200, [], '<h1>Bienvenue sur le blog</h1>');
        }
        return new Response(404, [], '<h1>Erreur 404</h1>');
    }
}
