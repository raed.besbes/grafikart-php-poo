<?php
use Framework\App;

//c quoi vendor authoload php

require '../vendor/autoload.php';

//see add authoload in composer json
$app = new App();
//generate automatic response
$response = $app->run(\GuzzleHttp\Psr7\ServerRequest::fromGlobals());
//il faut afficher le contenu et renvoyer les bons headers avec httpinterop
//mettre send ici au lieu que dans App faclitera les tests.
\Http\Response\send($response);
